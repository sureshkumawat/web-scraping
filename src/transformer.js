function transform(obj) {
	const playhouse = {
		brand: obj.name,
		address: obj.address,
		coverImage: obj.coverImage,
		phone: obj.phone,
		website: obj.website
	};
	return playhouse;
}

module.exports = transform;
