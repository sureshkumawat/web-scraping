const puppeteer = require("puppeteer");
const express = require("express");
const { connectDB } = require("./db.config");
const { Playhouse } = require("./model");
const { URLS } = require("./constants");
const transform = require("./transformer");

async function extractPlayhouse(page) {
	const playhouseInfo = {};

	await page.waitForSelector(".DUwDvf span");
	playhouseInfo["name"] = await page.$eval(
		".DUwDvf span",
		(el) => el.textContent
	);

	await page.waitForSelector(".aoRNLd img");
	playhouseInfo["coverImage"] = await page.$eval(".aoRNLd img", (el) => el.src);

	await page.waitForSelector('button[data-item-id="address"] .Io6YTe');
	playhouseInfo["address"] = await page.$eval(
		'button[data-item-id="address"] .Io6YTe',
		(el) => el.textContent
	);

	playhouseInfo["website"] = await page.evaluate(() => {
		const element = document.querySelector(
			'a[data-item-id="authority"] .Io6YTe'
		);
		return element ? element.textContent : "";
	});

	playhouseInfo["phone"] = await page.$eval(
		'button[data-tooltip="Copy phone number"] .Io6YTe',
		(el) => el.textContent
	);

	playhouseInfo["plusCode"] = await page.$eval(
		'button[data-item-id="oloc"] .Io6YTe',
		(el) => el.textContent
	);

	playhouseInfo["rating"] = await page.$eval(
		".jANrlb div",
		(el) => el.textContent
	);

	const reviews = await page.$eval(".HHrUdb", (el) => el.textContent);
	playhouseInfo["reviewsCount"] = parseInt(reviews.split(" ")[0]);

	return playhouseInfo;
}

async function getData(page, url) {
	const res = await page.goto(url);

	await page.waitForNavigation({ waitUntil: "networkidle2" });

	console.log(`Extracting... (status ${res.status()})`);
	const playhouse = await extractPlayhouse(page);

	console.log(`Adding "${playhouse.name}" to db...`);
	await Playhouse.create(transform(playhouse));
	console.log(`Added "${playhouse.name}" to db`);

	return playhouse;
}

async function main() {
	let browser;

	try {
		browser = await puppeteer.launch({ headless: false });
		const page = await browser.newPage();
		await page.setDefaultNavigationTimeout(40000);

		const data = [];

		for (url of URLS) {
			const res = await getData(page, url);
			data.push(res);
		}

		console.log("Data", data);
		console.log(`Successfully scraped ${data.length} playhouse`);
	} catch (error) {
		console.log(error);
	} finally {
		await browser.close();
	}
}
const app = express();
connectDB(app);

main();
