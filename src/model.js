const mongoose = require("mongoose");

const playhouseSchema = new mongoose.Schema({
	brand: {
		type: String,
		trim: true,
		maxLength: 125,
		required: [true, "{PATH} is required"]
	},
	address: {
		type: String,
		trim: true,
		maxLength: 250,
		required: [true, "{PATH} is required"]
	},
	coverImage: {
		type: String,
		trim: true,
		maxLength: [2048, "{PATH} is too long. Must be 2048 characters or less"],
		required: [true, "{PATH} is required"]
	},
	phone: {
		type: String,
		trim: true,
		maxLength: 15,
		required: [true, "{PATH} is required"]
	},
	website: {
		type: String,
		trim: true,
		maxLength: 125
	}
});

const Playhouse = mongoose.model("Playhouse", playhouseSchema);
module.exports = { Playhouse };
