const mongoose = require("mongoose");

const PORT = 5000;
const DB_URL = "mongodb://localhost:27017/scraping";

const connectDB = async (app) => {
	try {
		mongoose
			.connect(DB_URL, {
				useNewUrlParser: true,
				useUnifiedTopology: true
			})
			.then(() => {
				console.log("Connected to MongoDB");
				app.listen(PORT, () => {
					console.log(`PUDLE DISCOVERY SERVER: Listening to port ${PORT}`);
				});
			});
		process.on("SIGTERM", () => {
			if (process.env.NODE_ENV !== "development") {
				console.log("SIGTERM received");
			}
		});
	} catch (error) {
		console.error(`SIGTERM received: ${JSON.stringify(error)}`);
	}
};

module.exports = { connectDB };
